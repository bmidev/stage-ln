SYNOPSIS
========

This is a development environment package for setting up and running a basic
nginx server on Ubuntu 14.04.

Many of the following instructions are for running this package on Mac OS X, but
can also be adapted for Linux or Windows.



SECTIONS
========

This document contains the following sections:

+ Download the Environment Package
+ Preparing the Package for Usage
+ Commands for Using the Package
+ Deploying Project Files into the Environment
+ Setup The Host Computer
+ Copyright & License

The first time you setup an environment you must install all the dependencies
first. The "Setup The Host Computer" section contains the instructions for
installing all the dependencies. This only needs to be done once on the host
computer.



DOWNLOAD THE ENVIRONMENT PACKAGE
================================

```
mkdir new-stage
curl https://bitbucket.org/bmidev/stage-ln/get/master.tar.gz | tar -xz - -C new-stage --strip-components=1
mv new-stage project-id
cd project-id
```



PREPARING THE PACKAGE FOR USAGE
===============================

Review the settings in the Vagrantfile. These are pretty generic and should work
for most basic setups.



COMMANDS FOR USING THE PACKAGE
==============================

Start the Environment
---------------------
This command starts up the environment. If the VM hasn't been installed, then it
will install it. If environment hasn't been provisioned, then it will provision it.
```
vagrant up
```

SSH into the Environment
------------------------
This command will ssh into the environment.
```
vagrant ssh
```

Suspend the Environment
-----------------------
This command saves the state and suspends the environment. Suspended VMs can be
restarted very quickly.
```
vagrant suspend
```

Halt the Environment
--------------------
This command gracefully stops all services and then halts the VM. Halted VMs can
take a minute or to start up again.
```
vagrant halt
```

Run the Provisioner
-------------------
If you make changes to the environment after the first time it was started, then
you have to manually run the provision to apply the updates.
```
vagrant provision
```

Destroy the Environment
-----------------------
This command will destroy all the files related to VM powering the environment. It
will not destroy any of the configuration or source files in the project.
```
vagrant destroy
```



DEPLOYING PROJECT FILES INTO THE ENVIRONMENT
============================================

The environment will run project files from inside the Source folder of the
environment package. Any files placed inside it are automatically synchonized
into the VM that is powering the environmnet.

source/public is the document root directory nginx. Change the destination
directory deploy.local.sh to actual when you setup the stage. Then copy the
script into to your source code directory for ease of use.

```
deploy.local.sh
```



SETUP THE HOST COMPUTER
=======================

Install Virtual Box
-------------------
https://www.virtualbox.org/wiki/Downloads

Install Vagrant
---------------
https://www.vagrantup.com/downloads.html

Install Chef Solo
-----------------
```
curl -L https://www.opscode.com/chef/install.sh | sudo bash
```

Install the omnibus plugin for Vagrant
--------------------------------------
```
vagrant plugin install vagrant-omnibus
```



COPYRIGHT & LICENSE
===================

Copyright (c) 2015 Eric Lee Stewart (e.stewart@mac.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.