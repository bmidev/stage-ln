#
# Cookbook Name:: stack-builder
# Recipe:: default
#
# Copyright (C) 2015 Eric Stewart
#
# All rights reserved
#

# Install apt.
include_recipe 'apt'

# Install nginx.
include_recipe 'nginx'

# Install main nginx configuration.
cookbook_file "/etc/nginx/nginx.conf" do
  source "nginx.conf"
  owner 'root'
  group 'root'
  mode '0644'
  notifies :reload, "service[nginx]", :delayed
end

# Install project server configuration.
cookbook_file "/etc/nginx/conf.d/project.conf" do
  source "project.conf"
  owner 'root'
  group 'root'
  mode '0644'
  notifies :reload, "service[nginx]", :delayed
end
